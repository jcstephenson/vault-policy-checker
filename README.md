# Vault Policy Checker

This is a small utility to clarify vault policy file rule precedence
locally, without having to load into a running Vault instance.

This was built as I couldn't quickly find any tools to test this
behaviour, and is implemented best-effort according to the rule
precedence listed in the [relevant policy
documentation](https://developer.hashicorp.com/vault/docs/concepts/policies#policy-syntax).

## Usage

Options and usage are included in the `--help` text at each level.

The most useful feature is the `can-i` subcommand, which outputs
similarlry to the `kubectl` `auth can-i` subcommand, checking if you
can act on a path.

You must also specify a policy file to test against.

```
vault-policy-checker -f path/to/policy.hcl can-i create secrets/path/to/secret
```

You can also provide a `--debug` flag to get additional information, including which rule is matched.

## Installation

This is a rust utility that can be installed with `cargo`.

```
cargo install --git https://gitlab.com/jcstephenson/vault-policy-checker.git
```

or, if installing using locally cloned source.

```
cargo install --path .
```
