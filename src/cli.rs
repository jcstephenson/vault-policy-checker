use std::path::PathBuf;
use std::collections::BTreeMap;
use regex::Regex;
use serde::{Deserialize, Serialize};
use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[clap(subcommand)]
    command: Commands,

    /// Policy file to evaluate
    #[arg(short='f', long)]
    policy_file: PathBuf,

    #[arg(long, default_value="false")]
    debug: bool,
}

impl Cli {
    pub fn run() -> Result<(), Box<dyn std::error::Error>> {
        let cli = Self::parse();
        cli.command.run(&cli)
    }
}

#[derive(Subcommand, Debug)]
pub enum Commands {
    // /// Lint a policy file to ensure we won't hit any issues
    // Lint,
    /// Check permissions
    CanI {
	/// Action to evaluate
	action: String,
	/// Path to evaluate
	path: String,
    },
    /// Check if a path is matched in a file.
    ///
    /// This does not show the matched rule as it may not be the most
    /// specific rule, but shows that a path will be matched in the
    /// given policy.
    Matches {
	/// Path to evaluate against
	path: String,
    },
    /// Show the entries matches.
    #[clap(hide=true)]
    Entries,
    /// Dump the output of a file. Used for development and checking
    /// the structure of the HCL.
    #[clap(hide=true)]
    Dump,
}

impl Commands {
    fn run(&self, cli: &Cli) -> Result<(), Box<dyn std::error::Error>> {
        match self {
            // Commands::Lint => {
	    // 	let reader = std::fs::File::open(&cli.policy_file)?;
	    // 	// let contents: serde_json::Value = hcl::from_reader(reader)?;
	    // 	Ok(())
	    // },
	    Commands::CanI { action, path } => {
		let reader = std::fs::File::open(&cli.policy_file)?;
		let contents: HCLRoot = hcl::from_reader(reader)?;
		let rules: Vec<Policy> = contents["path"].iter()
							 .map(|(path, contents)| Policy::from((path.as_ref(), contents)))
							 .collect();

		let mut matches: Vec<Policy> = rules.into_iter()
		     .filter(|rule| {
			 let m = rule.matches(path);
			 m.is_ok() && m.unwrap()
		     })
		     .collect();

		if matches.is_empty() {
		    eprintln!("No rule matched");
		    return Ok(())
		}

		matches.sort();
		matches.reverse();

		let matched_rule = &matches[0];

		if cli.debug {
		    eprintln!("Rule matched: {}", matched_rule.path)
		}

		if matched_rule.capabilities.contains(&action.to_lowercase()) {
		    println!("yes");
		} else {
		    println!("no");
		}

		Ok(())
	    },
	    Commands::Matches { path } => {
		let reader = std::fs::File::open(&cli.policy_file)?;
		let contents: HCLRoot = hcl::from_reader(reader)?;
		let rules: Vec<Policy> = contents["path"].iter()
							 .map(|(path, contents)| Policy::from((path.as_ref(), contents)))
							 .collect();

		for rule in rules {
		    if rule.matches(path)? {
			println!("Rule matched in policy file.");
			if cli.debug {
			    eprintln!("Matched path is {}", rule.path);
			}
		    }
		}

		Ok(())
	    }
	    Commands::Entries => {
		let reader = std::fs::File::open(&cli.policy_file)?;
		let contents: HCLRoot = hcl::from_reader(reader)?;
		let rules: Vec<Policy> = contents["path"].iter()
							 .map(|(path, contents)| Policy::from((path.as_ref(), contents)))
							 .collect();

		rules.iter()
		     .for_each(|rule| {
			 println!("Matching '{}' gives capabilities '{:?}'", rule.path, rule.capabilities)
		     });
		Ok(())
	    },
	    Commands::Dump => {
		let reader = std::fs::File::open(&cli.policy_file)?;
		let contents: serde_json::Value = hcl::from_reader(reader)?;
		println!("{}", serde_json::to_string_pretty(&contents)?);
		Ok(())
	    }
        }
    }
}

pub type HCLRoot = BTreeMap<String, PolicyEntry>;
pub type PolicyEntry = BTreeMap<String, PolicyContents>;
#[derive(Serialize, Deserialize)]
pub struct PolicyContents {
    pub capabilities: Vec<String>,
}


/// Policy entry
#[derive(std::cmp::Eq, PartialEq)]
pub struct Policy {
    /// Path to match on
    pub path: String,
    /// Capabilities granted on the matching path for the user.
    pub capabilities: Vec<String>,
}

impl From<(&str, &PolicyContents)> for Policy {
    fn from(value: (&str, &PolicyContents)) -> Self {
	Policy {
	    path: value.0.to_owned(),
	    capabilities: value.1.capabilities.clone(),
	}
    }
}

impl PartialOrd for Policy {
    fn partial_cmp(&self, other: &Policy) -> Option<std::cmp::Ordering> {
       Some(self.cmp(other))
    }
}

impl Ord for Policy {
    /// 1. If the first wildcard (+) or glob (*) occurs earlier in P1, P1 is lower priority
    /// 2. If P1 ends in * and P2 doesn't, P1 is lower priority
    /// 3. If P1 has more + (wildcard) segments, P1 is lower priority
    /// 4. If P1 is shorter, it is lower priority
    /// 5. If P1 is smaller lexicographically, it is lower priority
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
	// Rule 1
	let wildcard_pos_self = self.path.find(|c: char| c == '*' || c == '+');
	let wildcard_pos_other = other.path.find(|c : char| c == '*' || c == '+');
	match (wildcard_pos_self, wildcard_pos_other) {
	    (Some(x), Some(y)) if x != y => { return x.cmp(&y) },
	    (Some(_), None) => { return std::cmp::Ordering::Less },
	    (None, Some(_)) => { return std::cmp::Ordering::Greater },
	    _ => eprintln!("Rule 1 not matched"),
	};

	// Rule 2
	let r2_ends_wildcard_self = self.path.ends_with('*');
	let r2_ends_wildcard_other = other.path.ends_with('*');
	match (r2_ends_wildcard_self, r2_ends_wildcard_other) {
	    (true, false) => { return std::cmp::Ordering::Less },
	    (false, true) => { return std::cmp::Ordering::Greater },
	    _ => eprintln!("Rule 2 not matched"),
	}

	// Rule 3
	let r3_wildcard_self = self.path.chars().filter(|c: &char| *c == '+').count();
	let r3_wildcard_other = other.path.chars().filter(|c: &char| *c == '+').count();
	match (r3_wildcard_self, r3_wildcard_other) {
	    (x, y) if x != y => { return x.cmp(&y).reverse() },
	    _ => eprintln!("Rule 3 not matched"),
	}

	// Rule 4
	match (self.path.len(), other.path.len()) {
	    (x, y) if x != y => { return x.cmp(&y) }
	    _ => eprintln!("Rule 4 not matched"),
	}

	// Rule 5
	let parts_self: Vec<&str> = self.path.split('/').collect();
	let parts_other: Vec<&str> = other.path.split('/').collect();
	match (parts_self.len(), parts_other.len()) {
	    (x, y) if x != y => { return x.cmp(&y) },
	    _ => eprintln!("Rule 5 not matched"),
	}

	std::cmp::Ordering::Equal
    }
}

#[derive(thiserror::Error, Debug)]
pub enum PolicyError {
    #[error("Failed to build regex")]
    RegexBuildError(#[from] regex::Error),
}

impl Policy {
    pub fn matches(&self, other: &str) -> Result<bool, PolicyError> {
	let path = self.path.replace('+', "[a-zA-Z0-9-_\\+ ]+");
	let path = path.replace('*', ".*");
	let path = format!("^{path}$");

	let new_re = Regex::new(&path)?;
	Ok(new_re.is_match(other))
    }
}
